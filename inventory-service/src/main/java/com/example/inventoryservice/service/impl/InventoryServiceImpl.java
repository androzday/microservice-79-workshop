package com.example.inventoryservice.service.impl;

import com.example.inventoryservice.dto.InventoryDto;
import com.example.inventoryservice.model.Inventory;
import com.example.inventoryservice.repository.InventoryRepository;
import com.example.inventoryservice.service.InventoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    public void save(Inventory inventory){
        Date date = new Date();
        inventory.setCreated_at(date);
        if(inventory.getId() == null){
            inventory.setId(UUID.randomUUID().toString());
            inventory.setUpdated_at(date);
        }
        inventoryRepository.save(inventory);

    }

    @Override
    public List<InventoryDto> findInventories(){
        List<Inventory> inventories = inventoryRepository.findAll();
        List<InventoryDto> inventoryDtos = new ArrayList<>();
        for(Inventory inventory:inventories){
            InventoryDto inventoryDto = new InventoryDto();
            BeanUtils.copyProperties(inventory,inventoryDto);
            inventoryDtos.add(inventoryDto);
        }
        return inventoryDtos;
    }
    
}
