package com.example.inventoryservice.service;

import com.example.inventoryservice.dto.InventoryDto;
import com.example.inventoryservice.model.Inventory;

import java.util.List;

public interface InventoryService {
    public void save(Inventory productService);

    public List<InventoryDto> findInventories();
}
