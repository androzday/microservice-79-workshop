package com.example.inventoryservice.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class InventoryDto {
    private String id;
    private String name;
    private String description;
    private BigDecimal price;
}
