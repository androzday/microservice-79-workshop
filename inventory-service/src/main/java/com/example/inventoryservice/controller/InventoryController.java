package com.example.inventoryservice.controller;

import com.example.inventoryservice.dto.InventoryDto;
import com.example.inventoryservice.model.Inventory;
import com.example.inventoryservice.service.InventoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(path = "/inventory")
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public Map<String,String> save(@RequestBody InventoryDto inventoryDto){
        Map<String,String> result = new HashMap<>();
        Inventory inventory = new Inventory();

        BeanUtils.copyProperties(inventoryDto,inventory);
        inventoryService.save(inventory);

        result.put("message", "success");
        return result;
        
    }

    @RequestMapping(path = "/find-all", method = RequestMethod.GET)
    public List<InventoryDto> findAll(){
         return inventoryService.findInventories();
    }
    
}
