package com.example.productservice.controller;

import com.example.productservice.dto.ProductDto;
import com.example.productservice.model.Product;
import com.example.productservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(path = "/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public Map<String,String> save(@RequestBody ProductDto productDto){
        Map<String,String> result = new HashMap<>();
        Product product = new Product();
        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());
        product.setDescription(productDto.getDescription());
        productService.save(product);

        result.put("message", "success");
        return result;
        
    }

    @RequestMapping(path = "/find-all", method = RequestMethod.GET)
    public List<ProductDto> findAll(){
         return productService.findProducts();
    }
    
}
