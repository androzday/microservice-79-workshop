package com.example.productservice.service.impl;

import com.example.productservice.dto.ProductDto;
import com.example.productservice.model.Product;
import com.example.productservice.repository.ProductRepository;
import com.example.productservice.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void save(Product product){
        Date date = new Date();
        product.setUpdated_at(date);
        if(product.getId() ==null){
            product.setId(UUID.randomUUID().toString());
            product.setCreated_at(date);
        }
        productRepository.save(product);

    }

    @Override
    public List<ProductDto> findProducts(){
        List<Product> products = productRepository.findAll();
        List<ProductDto> productDtos = new ArrayList<>();
        for(Product product:products){
            ProductDto productDto = new ProductDto();
            BeanUtils.copyProperties(product,productDto);
            productDtos.add(productDto);
        }
        return productDtos;
    }
    
}
