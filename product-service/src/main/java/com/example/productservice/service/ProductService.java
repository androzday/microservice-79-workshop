package com.example.productservice.service;

import com.example.productservice.dto.ProductDto;
import com.example.productservice.model.Product;

import java.util.List;

public interface ProductService {
    public void save(Product productService);

    public List<ProductDto> findProducts();
}
