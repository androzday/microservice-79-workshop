package com.microservices.orders.controller;

import com.microservices.orders.model.Orders;
import com.microservices.orders.service.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/orders")
public class OrdersController {

	@Autowired
	private OrdersRepository ordersRepository;

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<Orders> findAll() {
		return ordersRepository.findAll();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Orders create(@RequestBody Orders orders) {
		orders.setId(UUID.randomUUID().toString());
		return ordersRepository.save(orders);
	}

	@DeleteMapping
	@ResponseStatus(HttpStatus.OK)
	public void delete(@RequestParam String id) {
		Optional<Orders> orders = ordersRepository.findById(id);
		orders.ifPresent(value -> ordersRepository.delete(value));
	}
}
